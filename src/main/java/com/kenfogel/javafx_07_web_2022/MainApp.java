package com.kenfogel.javafx_07_web_2022;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

public class MainApp extends Application {

    /**
     * Start the JavaFX application
     *
     * @param primaryStage
     * @throws java.lang.Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Web Browser");
        Group root = new Group();
        root.getChildren().add(createWebView("https://omnijava.com"));
        primaryStage.setScene(new Scene(root, 1280, 768));
        primaryStage.show();
    }

    /**
     * Create the web view
     *
     * @param url
     * @return
     */
    private WebView createWebView(String url) {
        WebView webView = new WebView();
        webView.setMinWidth(1280);
        webView.setMinHeight(1024);
        WebEngine webEngine = webView.getEngine();
        webEngine.load(url);
        return webView;
    }

    /**
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
